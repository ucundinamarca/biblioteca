/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "both",
                centerStage: "both",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'Recurso_1',
                            type: 'image',
                            rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"Recurso_1.png",'0px','0px']
                        },
                        {
                            id: 'stage1',
                            type: 'group',
                            rect: ['209', '137', '606', '365', 'auto', 'auto'],
                            c: [
                            {
                                id: 'box',
                                type: 'group',
                                rect: ['0px', '0px', '606', '365', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'Recurso_2',
                                    type: 'image',
                                    rect: ['0px', '0px', '606px', '365px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_2.png",'0px','0px']
                                },
                                {
                                    id: 'aceptar_1',
                                    type: 'image',
                                    rect: ['232px', '297px', '142px', '52px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_3.png",'0px','0px']
                                },
                                {
                                    id: 'page_1',
                                    type: 'group',
                                    rect: ['27', '6px', '556', '241', 'auto', 'auto'],
                                    userClass: "ins",
                                    c: [
                                    {
                                        id: 'Text2Copy',
                                        type: 'text',
                                        rect: ['0px', '12px', '556px', '181px', 'auto', 'auto'],
                                        text: "<p style=\"margin: 0px; text-align: justify;\">​<span style=\"font-size: 18px;\">Este juego es un acto simbólico que busca que los participantes reflexionen sobre situaciones cotidianas que todos vivimos como servidores públicos, que nos parecen relativamente normales porque pasan muy a menudo, pero que realmente no son del todo íntegras y rompen con los principios de acción de los valores del Código de Integridad. Esta actividad de reflexión consiste en el reconocimiento, de parte de los servidores públicos, de ciertos malos hábitos que con el tiempo hemos normalizado. Una vez reconocidos, cada participante los identificará y desechará en el “archivo de los malos hábitos”, como símbolo de compromiso para reforzar la integridad en la Universidad.</span><span style=\"font-weight: 300; font-size: 21px;\"></span><span style=\"font-size: 18px;\">&nbsp;</span></p>",
                                        align: "left",
                                        userClass: "game",
                                        font: ['Arial, Helvetica, sans-serif', [22, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                        textStyle: ["", "", "22px", "", "none"]
                                    }]
                                },
                                {
                                    id: 'page_2',
                                    type: 'group',
                                    rect: ['27', '22', '556', '241', 'auto', 'auto'],
                                    userClass: "ins",
                                    c: [
                                    {
                                        id: 'Text',
                                        type: 'text',
                                        rect: ['52px', '0px', '460px', '45px', 'auto', 'auto'],
                                        text: "<p style=\"margin: 0px; text-align: center;\">​<span style=\"font-weight: 700;\">Instrucción</span></p>",
                                        userClass: "game",
                                        font: ['Arial, Helvetica, sans-serif', [24, ""], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                                    },
                                    {
                                        id: 'Text2',
                                        type: 'text',
                                        rect: ['0px', '54px', '556px', '181px', 'auto', 'auto'],
                                        text: "<p style=\"margin: 0px; text-align: justify;\">​<span style=\"font-size: 18px;\">Está a un paso de ser un servidor más íntegro. A continuación, visualizará una biblioteca de la cual usted elegirá un libro. En él encontrará los malos hábitos de los funcionarios públicos y responderá alguna preguntas relacionadas con su quehacer diario. Los malos hábitos serán lanzados al cesto de basura y los buenos serán conservados por usted en la cotidianidad. Recuerde hacerlo en el menor tiempo posible, y ser HONESTO Y DILIGENTE en este paso. </span><span style=\"font-weight: 300; font-size: 21px;\"></span><span style=\"font-size: 18px;\">&nbsp;</span></p>",
                                        align: "left",
                                        userClass: "game",
                                        font: ['Arial, Helvetica, sans-serif', [22, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                        textStyle: ["", "", "22px", "", "none"]
                                    }]
                                },
                                {
                                    id: 'icon1',
                                    type: 'ellipse',
                                    rect: ['284px', '279px', '12px', '13px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    borderRadius: ["50%", "50%", "50%", "50%"],
                                    fill: ["rgba(192,155,155,1.00)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"]
                                },
                                {
                                    id: 'icon2',
                                    type: 'ellipse',
                                    rect: ['309px', '279px', '12px', '13px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    borderRadius: ["50%", "50%", "50%", "50%"],
                                    fill: ["rgba(192,155,155,1.00)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"]
                                }]
                            }]
                        },
                        {
                            id: 'stage2',
                            type: 'group',
                            rect: ['209px', '137px', '606', '365', 'auto', 'auto'],
                            c: [
                            {
                                id: 'grupo',
                                type: 'group',
                                rect: ['0', '0', '606', '365', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'msg2',
                                    type: 'image',
                                    rect: ['0px', '0px', '606px', '365px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_2.png",'0px','0px']
                                },
                                {
                                    id: 'a2',
                                    type: 'image',
                                    rect: ['338px', '118px', '85px', '85px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_18.png",'0px','0px'],
                                    userClass: "a"
                                },
                                {
                                    id: 'a1',
                                    type: 'image',
                                    rect: ['191px', '118px', '82px', '83px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_15.png",'0px','0px'],
                                    userClass: "a"
                                },
                                {
                                    id: 'TextCopy',
                                    type: 'text',
                                    rect: ['79px', '22px', '460px', '45px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px; text-align: center;\">​<span style=\"font-weight: 700;\">Elige un avatar</span></p>",
                                    userClass: "game",
                                    font: ['Arial, Helvetica, sans-serif', [24, ""], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                                }]
                            }]
                        },
                        {
                            id: 'stage3',
                            type: 'group',
                            rect: ['0px', '-1px', '1024', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Recurso_4',
                                type: 'image',
                                rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_4.png",'0px','0px']
                            },
                            {
                                id: 'papel',
                                symbolName: 'papel',
                                type: 'rect',
                                rect: ['909', '566', '59', '45', 'auto', 'auto']
                            },
                            {
                                id: 'btn_launch',
                                symbolName: 'btn_launch',
                                type: 'rect',
                                rect: ['126', '262', '45', '110', 'auto', 'auto'],
                                cursor: 'pointer'
                            },
                            {
                                id: 'btn_ins',
                                type: 'image',
                                rect: ['48px', '41px', '169px', '50px', 'auto', 'auto'],
                                overflow: 'hidden',
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"Recurso_5.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_6',
                                type: 'image',
                                rect: ['455px', '29px', '408px', '125px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_6.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_7',
                                type: 'image',
                                rect: ['883px', '29px', '128px', '125px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_7.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_13',
                                type: 'image',
                                rect: ['909px', '566px', '45px', '58px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_13.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_8',
                                type: 'image',
                                rect: ['891px', '531px', '95px', '103px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_8.png",'0px','0px']
                            },
                            {
                                id: 'a2__',
                                type: 'image',
                                rect: ['470px', '50px', '85px', '85px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"Recurso_20.png",'0px','0px'],
                                userClass: "fotos"
                            },
                            {
                                id: 'a1__',
                                type: 'image',
                                rect: ['470px', '50px', '82px', '83px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"Recurso_15.png",'0px','0px'],
                                userClass: "fotos"
                            },
                            {
                                id: 'Text3',
                                type: 'text',
                                rect: ['561px', '47px', 'auto', 'auto', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\">​<span style=\"font-weight: 900;\">Jugador</span></p>",
                                userClass: "game",
                                font: ['Arial, Helvetica, sans-serif', [24, ""], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "nowrap"]
                            },
                            {
                                id: 'Text3Copy',
                                type: 'text',
                                rect: ['713px', '47px', 'auto', 'auto', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\">​<span style=\"font-weight: 900;\">Pregunta</span></p>",
                                userClass: "game",
                                font: ['Arial, Helvetica, sans-serif', [24, ""], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "nowrap"]
                            },
                            {
                                id: 'Text3Copy2',
                                type: 'text',
                                rect: ['899px', '47px', 'auto', 'auto', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\">​<span style=\"font-weight: 900;\">Puntaje</span></p>",
                                userClass: "game",
                                font: ['Arial, Helvetica, sans-serif', [24, ""], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "nowrap"]
                            },
                            {
                                id: 'Text3Copy3',
                                type: 'text',
                                rect: ['745px', '84px', 'auto', 'auto', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\">​<span style=\"font-weight: 900;\">de</span></p>",
                                userClass: "game",
                                font: ['Arial, Helvetica, sans-serif', [20, "px"], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "nowrap"]
                            },
                            {
                                id: 'Text3Copy4',
                                type: 'text',
                                rect: ['783px', '84px', '59px', '61px', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\">​<span style=\"font-weight: 900; font-size: 40px;\">23</span></p>",
                                align: "center",
                                userClass: "game",
                                font: ['Arial, Helvetica, sans-serif', [30, "px"], "rgba(0,0,0,1)", "normal", "none", "", "break-word", ""]
                            },
                            {
                                id: 'p',
                                type: 'text',
                                rect: ['916px', '84px', '59px', '61px', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\">​<span style=\"font-weight: 900; font-size: 40px;\">15</span></p>",
                                align: "center",
                                userClass: "game",
                                font: ['Arial, Helvetica, sans-serif', [30, "px"], "rgba(0,0,0,1)", "normal", "none", "", "break-word", ""]
                            },
                            {
                                id: 'n',
                                type: 'text',
                                rect: ['679px', '84px', '59px', '61px', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\">​<span style=\"font-weight: 900; font-size: 40px;\">1</span></p>",
                                align: "center",
                                userClass: "game",
                                font: ['Arial, Helvetica, sans-serif', [30, "px"], "rgba(0,0,0,1)", "normal", "none", "", "break-word", ""]
                            },
                            {
                                id: 'resma',
                                type: 'image',
                                rect: ['858px', '417px', '83px', '14px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"resma.png",'0px','0px']
                            },
                            {
                                id: 'Group',
                                type: 'group',
                                rect: ['-213px', '154', '1086', '330', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'Group5',
                                    type: 'group',
                                    rect: ['0px', '0px', '1086', '330', 'auto', 'auto'],
                                    c: [
                                    {
                                        id: 'Recurso_12',
                                        type: 'image',
                                        rect: ['0px', '263px', '83px', '14px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_12.png",'0px','0px']
                                    },
                                    {
                                        id: 'Recurso_9',
                                        type: 'image',
                                        rect: ['635px', '0px', '451px', '330px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_9.png",'0px','0px']
                                    },
                                    {
                                        id: 'si',
                                        type: 'image',
                                        rect: ['784px', '270px', '71px', '32px', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"si.png",'0px','0px'],
                                        filter: [0, 0, 1, 1, 0, 0, 1, 0, "rgba(0,0,0,0)", 0, 0, 0]
                                    },
                                    {
                                        id: 'no',
                                        type: 'image',
                                        rect: ['882px', '270px', '71px', '32px', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"no.png",'0px','0px'],
                                        filter: [0, 0, 1, 1, 0, 1, 1, 0, "rgba(0,0,0,0)", 0, 0, 0]
                                    }]
                                },
                                {
                                    id: 'q',
                                    type: 'text',
                                    rect: ['660px', '89px', '400px', '148px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px;\">​hhh</p><p style=\"margin: 0px;\">​</p>",
                                    align: "left",
                                    userClass: "game",
                                    font: ['Arial, Helvetica, sans-serif', [21, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                    textStyle: ["", "", "", "", "none"]
                                },
                                {
                                    id: 'a1_',
                                    type: 'group',
                                    rect: ['541px', '192px', '112', '288', 'auto', 'auto'],
                                    userClass: "fotos",
                                    c: [
                                    {
                                        id: 'Recurso_32',
                                        type: 'image',
                                        rect: ['13px', '267px', '109px', '25px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_32.png",'0px','0px']
                                    },
                                    {
                                        id: 'Recurso_27',
                                        type: 'image',
                                        rect: ['0px', '0px', '112px', '288px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_27.png",'0px','0px']
                                    },
                                    {
                                        id: 'avatar1',
                                        symbolName: 'head1',
                                        type: 'rect',
                                        rect: ['33', '-77', '59', '86', 'auto', 'auto']
                                    }]
                                },
                                {
                                    id: 'a2_',
                                    type: 'group',
                                    rect: ['549px', '115px', '111', '369', 'auto', 'auto'],
                                    userClass: "fotos",
                                    c: [
                                    {
                                        id: 'avatar2',
                                        symbolName: 'avatar2',
                                        type: 'rect',
                                        rect: ['0px', '0px', '111', '369', 'auto', 'auto']
                                    }]
                                }]
                            }]
                        },
                        {
                            id: 'preload',
                            type: 'group',
                            rect: ['0', '0', '1024', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Rectangle3',
                                type: 'rect',
                                rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,1)"],
                                stroke: [0,"rgb(0, 0, 0)","none"]
                            },
                            {
                                id: 'logo',
                                type: 'image',
                                rect: ['462px', '272px', '100px', '100px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"logo.svg",'0px','0px']
                            }]
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1024px', '640px', 'auto', 'auto'],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 7000,
                    autoPlay: true,
                    data: [

                    ]
                }
            },
            "head1": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            rect: ['0', '0', '59', '86', 'auto', 'auto'],
                            id: 'Group3',
                            type: 'group',
                            transform: [[], ['-6'], [0, 0, 0], [1, 1, 1]],
                            c: [
                            {
                                type: 'image',
                                id: 'cabeza',
                                rect: ['0px', '0px', '59px', '86px', 'auto', 'auto'],
                                fill: ['rgba(0,0,0,0)', 'images/Recurso_110.png', '0px', '0px']
                            },
                            {
                                type: 'group',
                                id: 'ojos',
                                rect: ['9px', '41px', '36', '9', 'auto', 'auto'],
                                c: [
                                {
                                    rect: ['11px', '25px', '18px', '9px', 'auto', 'auto'],
                                    id: 'voca_fin',
                                    type: 'image',
                                    display: 'block',
                                    fill: ['rgba(0,0,0,0)', 'images/Recurso_92.png', '0px', '0px']
                                },
                                {
                                    rect: ['11px', '25px', '16px', '8px', 'auto', 'auto'],
                                    id: 'voca_fail',
                                    type: 'image',
                                    display: 'none',
                                    fill: ['rgba(0,0,0,0)', 'images/Recurso_82.png', '0px', '0px']
                                },
                                {
                                    rect: ['9px', '3px', '25px', '5px', 'auto', 'auto'],
                                    type: 'image',
                                    id: 'ojos_normal',
                                    opacity: '1',
                                    display: 'block',
                                    fill: ['rgba(0,0,0,0)', 'images/Recurso_72.png', '0px', '0px']
                                },
                                {
                                    rect: ['9px', '3px', '25px', '6px', 'auto', 'auto'],
                                    id: 'ojos_good',
                                    type: 'image',
                                    display: 'none',
                                    fill: ['rgba(0,0,0,0)', 'images/Recurso_62.png', '0px', '0px']
                                },
                                {
                                    rect: ['4px', '-5px', '36px', '6px', 'auto', 'auto'],
                                    id: 'cejas_win',
                                    type: 'image',
                                    display: 'block',
                                    fill: ['rgba(0,0,0,0)', 'images/Recurso_52.png', '0px', '0px']
                                },
                                {
                                    rect: ['3px', '-6px', '36px', '5px', 'auto', 'auto'],
                                    id: 'cejas_fail',
                                    type: 'image',
                                    display: 'none',
                                    fill: ['rgba(0,0,0,0)', 'images/Recurso_42.png', '0px', '0px']
                                }]
                            }]
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '59px', '86px']
                        }
                    }
                },
                timeline: {
                    duration: 7000,
                    autoPlay: true,
                    labels: {
                        "stand_by": 0,
                        "good": 1000,
                        "fail": 4000
                    },
                    data: [
                        [
                            "eid36",
                            "display",
                            4000,
                            0,
                            "linear",
                            "${voca_fin}",
                            'block',
                            'none'
                        ],
                        [
                            "eid20",
                            "-webkit-transform-origin",
                            0,
                            0,
                            "linear",
                            "${Group3}",
                            [50,50],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid684",
                            "-moz-transform-origin",
                            0,
                            0,
                            "linear",
                            "${Group3}",
                            [50,50],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid685",
                            "-ms-transform-origin",
                            0,
                            0,
                            "linear",
                            "${Group3}",
                            [50,50],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid686",
                            "msTransformOrigin",
                            0,
                            0,
                            "linear",
                            "${Group3}",
                            [50,50],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid687",
                            "-o-transform-origin",
                            0,
                            0,
                            "linear",
                            "${Group3}",
                            [50,50],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid688",
                            "transform-origin",
                            0,
                            0,
                            "linear",
                            "${Group3}",
                            [50,50],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid30",
                            "display",
                            4000,
                            0,
                            "linear",
                            "${cejas_win}",
                            'block',
                            'none'
                        ],
                        [
                            "eid8",
                            "scaleY",
                            1000,
                            1000,
                            "linear",
                            "${Group3}",
                            '1',
                            '1.05'
                        ],
                        [
                            "eid10",
                            "scaleY",
                            2000,
                            1000,
                            "linear",
                            "${Group3}",
                            '1.05',
                            '1'
                        ],
                        [
                            "eid4",
                            "skewY",
                            1000,
                            0,
                            "linear",
                            "${Group3}",
                            '0deg',
                            '0deg'
                        ],
                        [
                            "eid5",
                            "skewY",
                            2000,
                            0,
                            "linear",
                            "${Group3}",
                            '0deg',
                            '0deg'
                        ],
                        [
                            "eid23",
                            "rotateZ",
                            4000,
                            500,
                            "linear",
                            "${Group3}",
                            '0deg',
                            '6deg'
                        ],
                        [
                            "eid25",
                            "rotateZ",
                            4500,
                            1000,
                            "linear",
                            "${Group3}",
                            '6deg',
                            '-6deg'
                        ],
                        [
                            "eid27",
                            "rotateZ",
                            5500,
                            1000,
                            "linear",
                            "${Group3}",
                            '-6deg',
                            '6deg'
                        ],
                        [
                            "eid29",
                            "rotateZ",
                            6500,
                            500,
                            "linear",
                            "${Group3}",
                            '6deg',
                            '0deg'
                        ],
                        [
                            "eid14",
                            "display",
                            1000,
                            0,
                            "linear",
                            "${ojos_normal}",
                            'block',
                            'block'
                        ],
                        [
                            "eid16",
                            "display",
                            1250,
                            0,
                            "linear",
                            "${ojos_normal}",
                            'block',
                            'none'
                        ],
                        [
                            "eid34",
                            "display",
                            4000,
                            0,
                            "linear",
                            "${ojos_normal}",
                            'none',
                            'block'
                        ],
                        [
                            "eid15",
                            "opacity",
                            1250,
                            0,
                            "linear",
                            "${ojos_normal}",
                            '1',
                            '1'
                        ],
                        [
                            "eid33",
                            "opacity",
                            4000,
                            0,
                            "linear",
                            "${ojos_normal}",
                            '1',
                            '1'
                        ],
                        [
                            "eid11",
                            "display",
                            0,
                            0,
                            "linear",
                            "${cejas_fail}",
                            'none',
                            'none'
                        ],
                        [
                            "eid31",
                            "display",
                            4000,
                            0,
                            "linear",
                            "${cejas_fail}",
                            'none',
                            'block'
                        ],
                        [
                            "eid12",
                            "display",
                            0,
                            0,
                            "linear",
                            "${ojos_good}",
                            'none',
                            'none'
                        ],
                        [
                            "eid17",
                            "display",
                            1250,
                            0,
                            "linear",
                            "${ojos_good}",
                            'none',
                            'block'
                        ],
                        [
                            "eid32",
                            "display",
                            4000,
                            0,
                            "linear",
                            "${ojos_good}",
                            'block',
                            'none'
                        ],
                        [
                            "eid13",
                            "display",
                            0,
                            0,
                            "linear",
                            "${voca_fail}",
                            'none',
                            'none'
                        ],
                        [
                            "eid35",
                            "display",
                            4000,
                            0,
                            "linear",
                            "${voca_fail}",
                            'none',
                            'block'
                        ]
                    ]
                }
            },
            "avatar2": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            type: 'group',
                            id: 'avarar2',
                            rect: ['0px', '75px', '110', '282', 'auto', 'auto'],
                            c: [
                            {
                                type: 'image',
                                id: 'sombra-personaje2',
                                rect: ['7px', '267px', '104px', '27px', 'auto', 'auto'],
                                fill: ['rgba(0,0,0,0)', 'images/sombra-personaje2.png', '0px', '0px']
                            },
                            {
                                transform: [[], ['-4'], [0, 0, 0], [1, 1, 1]],
                                id: 'pelo',
                                type: 'image',
                                rect: ['25px', '-36px', '59px', '83px', 'auto', 'auto'],
                                fill: ['rgba(0,0,0,0)', 'images/Recurso_3_fm.png', '0px', '0px']
                            },
                            {
                                type: 'image',
                                id: 'body1',
                                rect: ['0px', '0px', '110px', '282px', 'auto', 'auto'],
                                fill: ['rgba(0,0,0,0)', 'images/Recurso_1_fm.png', '0px', '0px']
                            },
                            {
                                rect: ['22px', '-77px', '67px', '89px', 'auto', 'auto'],
                                type: 'rect',
                                id: 'Rectangle',
                                stroke: [0, 'rgba(0,0,0,1)', 'none'],
                                fill: ['rgba(192,192,192,0.00)'],
                                c: [
                                {
                                    type: 'image',
                                    id: 'cabeza1',
                                    rect: ['1px', '2px', '61px', '79px', 'auto', 'auto'],
                                    fill: ['rgba(0,0,0,0)', 'images/Recurso_4_fm.png', '0px', '0px']
                                },
                                {
                                    rect: ['14px', '43px', '34px', '7px', 'auto', 'auto'],
                                    id: 'ojos_good',
                                    type: 'image',
                                    display: 'none',
                                    fill: ['rgba(0,0,0,0)', 'images/Recurso_10_fm.png', '0px', '0px']
                                },
                                {
                                    rect: ['15px', '43px', '34px', '7px', 'auto', 'auto'],
                                    id: 'ojos_fail',
                                    type: 'image',
                                    display: 'block',
                                    fill: ['rgba(0,0,0,0)', 'images/Recurso_9_fm.png', '0px', '0px']
                                },
                                {
                                    rect: ['24px', '67px', '16px', '3px', 'auto', 'auto'],
                                    id: 'boca_fail',
                                    type: 'image',
                                    display: 'none',
                                    fill: ['rgba(0,0,0,0)', 'images/Recurso_8_fm.png', '0px', '0px']
                                },
                                {
                                    rect: ['24px', '64px', '17px', '10px', 'auto', 'auto'],
                                    id: 'boca_good',
                                    type: 'image',
                                    display: 'block',
                                    fill: ['rgba(0,0,0,0)', 'images/Recurso_7_fm.png', '0px', '0px']
                                },
                                {
                                    rect: ['13px', '35px', '36px', '6px', 'auto', 'auto'],
                                    id: 'cejas_fail',
                                    type: 'image',
                                    display: 'none',
                                    fill: ['rgba(0,0,0,0)', 'images/Recurso_6_fm.png', '0px', '0px']
                                },
                                {
                                    rect: ['14px', '37px', '36px', '6px', 'auto', 'auto'],
                                    id: 'cejas_normal',
                                    type: 'image',
                                    display: 'block',
                                    fill: ['rgba(0,0,0,0)', 'images/Recurso_5_fm.png', '0px', '0px']
                                }]
                            }]
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '111px', '369px']
                        }
                    }
                },
                timeline: {
                    duration: 6500,
                    autoPlay: true,
                    labels: {
                        "good": 1000,
                        "fail": 3500
                    },
                    data: [
                        [
                            "eid47",
                            "display",
                            0,
                            0,
                            "linear",
                            "${cejas_fail}",
                            'none',
                            'none'
                        ],
                        [
                            "eid102",
                            "display",
                            3500,
                            0,
                            "linear",
                            "${cejas_fail}",
                            'none',
                            'block'
                        ],
                        [
                            "eid49",
                            "display",
                            0,
                            0,
                            "linear",
                            "${ojos_fail}",
                            'block',
                            'block'
                        ],
                        [
                            "eid104",
                            "display",
                            1000,
                            0,
                            "linear",
                            "${ojos_fail}",
                            'block',
                            'none'
                        ],
                        [
                            "eid106",
                            "display",
                            3500,
                            0,
                            "linear",
                            "${ojos_fail}",
                            'none',
                            'block'
                        ],
                        [
                            "eid116",
                            "display",
                            3500,
                            0,
                            "linear",
                            "${boca_good}",
                            'block',
                            'none'
                        ],
                        [
                            "eid101",
                            "display",
                            3500,
                            0,
                            "linear",
                            "${cejas_normal}",
                            'block',
                            'none'
                        ],
                        [
                            "eid48",
                            "display",
                            0,
                            0,
                            "linear",
                            "${boca_fail}",
                            'none',
                            'none'
                        ],
                        [
                            "eid117",
                            "display",
                            3500,
                            0,
                            "linear",
                            "${boca_fail}",
                            'none',
                            'block'
                        ],
                        [
                            "eid140",
                            "left",
                            0,
                            0,
                            "linear",
                            "${pelo}",
                            '25px',
                            '25px'
                        ],
                        [
                            "eid109",
                            "rotateZ",
                            3500,
                            500,
                            "linear",
                            "${pelo}",
                            '0deg',
                            '7deg'
                        ],
                        [
                            "eid111",
                            "rotateZ",
                            4000,
                            1000,
                            "linear",
                            "${pelo}",
                            '7deg',
                            '-7deg'
                        ],
                        [
                            "eid113",
                            "rotateZ",
                            5000,
                            1000,
                            "linear",
                            "${pelo}",
                            '-7deg',
                            '8deg'
                        ],
                        [
                            "eid115",
                            "rotateZ",
                            6000,
                            500,
                            "linear",
                            "${pelo}",
                            '8deg',
                            '0deg'
                        ],
                        [
                            "eid68",
                            "left",
                            0,
                            0,
                            "linear",
                            "${ojos_fail}",
                            '15px',
                            '15px'
                        ],
                        [
                            "eid81",
                            "top",
                            1000,
                            1000,
                            "linear",
                            "${Rectangle}",
                            '-75px',
                            '-77px'
                        ],
                        [
                            "eid72",
                            "scaleY",
                            1000,
                            1000,
                            "linear",
                            "${Rectangle}",
                            '1',
                            '1.05'
                        ],
                        [
                            "eid74",
                            "scaleY",
                            2000,
                            1000,
                            "linear",
                            "${Rectangle}",
                            '1.05',
                            '1'
                        ],
                        [
                            "eid94",
                            "rotateZ",
                            3500,
                            500,
                            "linear",
                            "${Rectangle}",
                            '0deg',
                            '6deg'
                        ],
                        [
                            "eid96",
                            "rotateZ",
                            4000,
                            1000,
                            "linear",
                            "${Rectangle}",
                            '6deg',
                            '-6deg'
                        ],
                        [
                            "eid98",
                            "rotateZ",
                            5000,
                            1000,
                            "linear",
                            "${Rectangle}",
                            '-6deg',
                            '6deg'
                        ],
                        [
                            "eid100",
                            "rotateZ",
                            6000,
                            500,
                            "linear",
                            "${Rectangle}",
                            '6deg',
                            '0deg'
                        ],
                        [
                            "eid55",
                            "display",
                            0,
                            0,
                            "linear",
                            "${ojos_good}",
                            'none',
                            'none'
                        ],
                        [
                            "eid103",
                            "display",
                            1000,
                            0,
                            "linear",
                            "${ojos_good}",
                            'none',
                            'block'
                        ],
                        [
                            "eid105",
                            "display",
                            3500,
                            0,
                            "linear",
                            "${ojos_good}",
                            'block',
                            'none'
                        ],
                        [
                            "eid69",
                            "-webkit-transform-origin",
                            0,
                            0,
                            "linear",
                            "${Rectangle}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid689",
                            "-moz-transform-origin",
                            0,
                            0,
                            "linear",
                            "${Rectangle}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid690",
                            "-ms-transform-origin",
                            0,
                            0,
                            "linear",
                            "${Rectangle}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid691",
                            "msTransformOrigin",
                            0,
                            0,
                            "linear",
                            "${Rectangle}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid692",
                            "-o-transform-origin",
                            0,
                            0,
                            "linear",
                            "${Rectangle}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid693",
                            "transform-origin",
                            0,
                            0,
                            "linear",
                            "${Rectangle}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ]
                    ]
                }
            },
            "papel": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            type: 'image',
                            id: 'Recurso_14',
                            opacity: '1',
                            rect: ['8px', '-476px', '59px', '45px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', 'images/Recurso_14.png', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '59px', '45px']
                        }
                    }
                },
                timeline: {
                    duration: 4000,
                    autoPlay: true,
                    data: [
                        [
                            "eid398",
                            "opacity",
                            3000,
                            1000,
                            "easeInBack",
                            "${Recurso_14}",
                            '1',
                            '0'
                        ],
                        [
                            "eid344",
                            "top",
                            0,
                            2000,
                            "easeInBack",
                            "${Recurso_14}",
                            '-476px',
                            '0px'
                        ],
                        [
                            "eid345",
                            "left",
                            0,
                            0,
                            "linear",
                            "${Recurso_14}",
                            '8px',
                            '8px'
                        ]
                    ]
                }
            },
            "btn_launch": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            type: 'image',
                            id: 'resaltador',
                            opacity: '1',
                            rect: ['0px', '0px', '45px', '110px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', 'images/resaltador.png', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '45px', '110px']
                        }
                    }
                },
                timeline: {
                    duration: 1500,
                    autoPlay: true,
                    data: [
                        [
                            "eid491",
                            "opacity",
                            0,
                            1000,
                            "easeInBack",
                            "${resaltador}",
                            '1',
                            '0'
                        ],
                        [
                            "eid493",
                            "opacity",
                            1000,
                            500,
                            "easeInBack",
                            "${resaltador}",
                            '0',
                            '1'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("biblioteca_edgeActions.js");
})("EDGE-361118490");
