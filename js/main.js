
var ST = "#Stage_";
var S = "Stage_";
ivo.info({
    title: "Biblioteca",
    autor: "Edilson Laverde Molina",
    date:  "10/03/2021",
    email: "edilsonlaverde_182@hotmail.com",
    icon: 'https://www.ucundinamarca.edu.co/images/iconos/favicon_verde_1.png',
    facebook: "https://www.facebook.com/edilson.laverde.molina"
});
var audios=[
    {url:"sonidos/click.mp3",      name:"clic" },
    {url:"sonidos/fail.mp3",       name:"fail" },
    {url:"sonidos/good.mp3",       name:"good" },
    {url:"sonidos/papel.mp3",       name:"papel" },
];
index=0;
var preguntas=[
    {q:"1. He revisado Facebook en la oficina."},
    {q:"2. He usado la impresora de la oficina para imprimir archivos personales."},
    {q:"3. He culpado al trancón por llegar tarde."},
    {q:"4. He inventado un compromiso familiar o una cita médica para salir temprano."},
    {q:"5. He sacado excusas para justificar no terminar un trabajo."},
    {q:"6. He citado a reuniones innecesarias."},
    {q:"7. He llegado tarde a una reunión."},
    {q:"8. He chateado durante una reunión."},
    {q:"9. He hablado mal de un compañero de trabajo."},
    {q:"10. He criticado a mi jefe."},
    {q:"11. He terminado un trabajo a las carreras."},
    {q:"12. He usado el tiempo de la oficina para terminar un trabajo personal."},
    {q:"13. He evadido responsabilidades."},
    {q:"14. He procrastinado (dejar un trabajo para terminar después)"},
    {q:"15. He usado el computador de la oficina para ver un partido de fútbol."},
    {q:"16. He echado al agua a alguien para justificar mis errores."},
    {q:"17. He ignorado un correo electrónico."},
    {q:"18. He peleado con un compañero o un ciudadano."},
    {q:"19. He hablado mal de mi entidad."},
    {q:"20. He ignorado un saludo."},
    {q:"21. He contratado o no contratado a alguien por su apariencia física."},
    {q:"22. He dado órdenes sin razón."},
    {q:"23. He contratado/apoyado la contratación de alguien que no cumpla con los requerimientos"}
];
var libro=[
    {
        "top" :"262px",
        "left":"126px"
    },
    {
        "top" :"160px",
        "left":"162px"
    },
    {
        "top" :"350px",
        "left":"40px"
    }
]
var indexL=0;
var primary=true;
var puntos=0;
var id="";
function main(sym) {

udec = ivo.structure({
        created:function(){
           t=this;
           ivo(ST+"m").text("0");
            //precarga audios//
           ivo.load_audio(audios,onComplete=function(){
               ivo(ST+"preload").hide();
               ivo(".ins").hide();
               ivo(ST+"page_1").show();
               t.events();
               t.animation();
               stage1.play();
           });
           Game_UdeC.info_moodle();
        },
        methods: {
            launch:function(){
                if(index<=22){
                    ivo(ST+"n p span").text(index+1);
                    ivo(ST+"q").html(`${preguntas[index].q}`);
                }else{
                    ivo(ST+"q").html(``);
                    ivo(ST+"si").hide();
                    ivo(ST+"no").hide();
                    
                }
            },
            events:function(){
                var t=this;
                ivo(ST+"icon1").on("click",function(){
                    ivo(".ins").hide();
                    ivo(ST+"page_1").show();
                    ivo.play("clic");
                });

                
                $(ST+"icon2").addClass("animated infinite tada");
                ivo(ST+"icon2").on("click",function(){
                    $(ST+"icon2").removeClass("animated infinite tada");
                    ivo(".ins").hide();
                    ivo(ST+"page_2").show();
                    ivo.play("clic");
                });

                ivo(ST+"aceptar_1").on("click",function(){
                    if(primary){
                        stage1.reverse().timeScale(3);
                        stage2.play().timeScale(1);
                        primary=false;
                    }else{
                        stage1.reverse().timeScale(3);
                        stage3.play().timeScale(1);
                    }
                    ivo.play("clic");
                });
                ivo(ST+"btn_ins").on("click",function(){
                    stage3.reverse().timeScale(3);
                    stage1.play().timeScale(1);
                    ivo.play("clic");
                });

                ivo(ST+"si").on("click",function(){
                    preguntas[index].rta="Si";
                    index+=1;
                    if(index>=23){
                        

                        Swal.fire({
                            title: '<strong>Retroalimentación</strong>',imageUrl: './images/home.png',
                            imageHeight: 100,
                            imageAlt: 'A tall image',
                            showCancelButton: false,
                            showConfirmButton: false,
                            html:
                              'Recuerde que el servir es un acto humano y que los valores influyen en nuestros actos, por lo que brindan mérito y reconocimiento a quienes los ejecutan bien contemplando pautas correctas de conducta individual. El ser servidor público implica respeto, responsabilidad y compromiso no solo para con la institución, sino para con la comunidad y la nación. ' ,
                            
                          }); 
                          ivo(".swal2-image").on("click",function(){
                            window.location.href="https://virtual.ucundinamarca.edu.co/course/view.php?id=15147&section=1";
                          }).css("cursor","pointer");
                        Game_UdeC.data.quiz = preguntas;
                        Game_UdeC.data.game = "Biblioteca";
                        Game_UdeC.data.url_game = 'https://virtual.ucundinamarca.edu.co/red/Otros/games/https://virtual.ucundinamarca.edu.co/red/Otros/games/biblioteca/biblioteca.html';
                        Game_UdeC.save();
                        
                    }
                    ivo.play("fail");
                    fail.volume(.2);
                    sym.getSymbol("avatar"+id).play("fail");
                    sym.getSymbol("papel").play(0);
                    papel.play();
                    setTimeout(function(){
                        Group.reverse();
                        ivo(ST+"btn_launch").show();
                    },3000);
                    indexL+=1;
                    ivo(ST+"btn_launch").css("top" , libro[indexL].top );
                    ivo(ST+"btn_launch").css("left", libro[indexL].left);
                    if(indexL==2){indexL=-1}
                });
                ivo(ST+"no").on("click",function(){
                    preguntas[index].rta="No";
                    index+=1;
                    if(index>=23){
                        Swal.fire({
                            title: '<strong>Retroalimentación</strong>',imageUrl: './images/home.png',
                            imageHeight: 100,
                            imageAlt: 'A tall image',
                            showCancelButton: false,
                            showConfirmButton: false,
                            html:
                              'Recuerde que el servir es un acto humano y que los valores influyen en nuestros actos, por lo que brindan mérito y reconocimiento a quienes los ejecutan bien contemplando pautas correctas de conducta individual. El ser servidor público implica respeto, responsabilidad y compromiso no solo para con la institución, sino para con la comunidad y la nación. ' ,
                            
                          }); 
                          ivo(".swal2-image").on("click",function(){
                            window.location.href="https://virtual.ucundinamarca.edu.co/course/view.php?id=15147&section=1";
                          }).css("cursor","pointer");
                        Game_UdeC.data.quiz = preguntas;
                        Game_UdeC.data.game = "Biblioteca";
                        Game_UdeC.data.url_game = 'https://virtual.ucundinamarca.edu.co/red/Otros/games/https://virtual.ucundinamarca.edu.co/red/Otros/games/biblioteca/biblioteca.html';
                        Game_UdeC.save();
                    }
                    ivo.play("good");
                    sym.getSymbol("avatar"+id).play("good");
                    puntos+=1;
                    ivo(ST+"p p span").text(puntos);
                    setTimeout(function(){
                        Group.reverse();
                        ivo(ST+"btn_launch").show();
                    },3000);
                    
                    indexL+=1;
                    ivo(ST+"btn_launch").css("top" , libro[indexL].top );
                    ivo(ST+"btn_launch").css("left", libro[indexL].left);
                    if(indexL==2){indexL=-1}
                });

                ivo(ST+"btn_launch").on("click",function(){
                    t.launch();
                    ivo.play("clic");
                    Group.play();
                    ivo(ST+"btn_launch").hide();
                });
                
                ivo(".a").on("click",function(){
                    id=ivo(this).attr("id").split(S+"a")[1];
                    let id1 = "#"+ivo(this).attr("id")+"_";
                    let id2 = "#"+ivo(this).attr("id")+"__";
                    stage2.reverse().timeScale(4);
                    stage3.play().timeScale(1);
                    ivo.play("clic");
                    ivo(".fotos").hide();
                    ivo(id1).show();
                    ivo(id2).show();
                });
            },
            animation:function(){
                stage1 = new TimelineMax();
                stage1.append(TweenMax.from(ST+"stage1", .8,          {x:1300,opacity:0}), 0);
                stage1.append(TweenMax.from(ST+"msg", .8,             {x:1300,opacity:0}), 0);
                stage1.stop();

                stage2 = new TimelineMax();
                stage2.append(TweenMax.from(ST+"stage2", .8,          {x:1300,opacity:0}), 0);
                stage2.append(TweenMax.from(ST+"grupo", .8,           {x:1300,opacity:0}), 0);
                stage2.stop();

                stage3 = new TimelineMax();
                stage3.append(TweenMax.fromTo(ST+"stage3", .4,        {opacity:0,y:900},{opacity:1,y:0}), 0);
                stage3.stop();

                stage4 = new TimelineMax();
                stage4.append(TweenMax.fromTo(ST+"stage4", .4,        {opacity:0,y:900},{opacity:1,y:0}), 0);
                stage4.stop();

                Group = new TimelineMax();
                Group.append(TweenMax.fromTo(ST+"Group", .4,        {opacity:0,y:900},{opacity:1,y:0}), 0);
                Group.stop();

            }
        }
 });
}
