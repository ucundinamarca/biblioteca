function cronometro(time,f){
    var reloj= setInterval(function(){
        
        if(time==0){
            clearInterval(reloj);
        }
        let label="";
        if(time<10){
            label="0"+time;
        }else{
            label=time;
        }
        f(label);
        time=time-1;
    },1000)
}