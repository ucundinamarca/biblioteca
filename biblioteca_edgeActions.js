/***********************
* Acciones de composición de Adobe Edge Animate
*
* Editar este archivo con precaución, teniendo cuidado de conservar 
* las firmas de función y los comentarios que comienzan con "Edge" para mantener la 
* capacidad de interactuar con estas acciones en Adobe Edge Animate
*
***********************/
(function($, Edge, compId){
var Composition = Edge.Composition, Symbol = Edge.Symbol; // los alias más comunes para las clases de Edge

   //Edge symbol: 'stage'
   (function(symbolName) {
      
      
      Symbol.bindElementAction(compId, symbolName, "document", "compositionReady", function(sym, e) {
         // introducir aquí código que se debe ejecutar cuando la composición está totalmente cargada
         yepnope({
         	nope:[
         		'css/udec.css',
               'js/sweetalert.min.js',
               'js/jquery-3.3.1.min.js',
               'js/ivo.js',
               'js/TweenMax.min.js',
               'js/howler.min.js',
               'js/game.js',
               'js/main.js'
         	],
            complete: init
         }); // end of yepnope
         function init() {
         	main(sym);
         }

      });
      //Edge binding end

   })("stage");
   //Edge symbol end:'stage'

   //=========================================================
   
   //Edge symbol: 'head1'
   (function(symbolName) {   
   
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1000, function(sym, e) {
         // introducir código aquí
      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 3000, function(sym, e) {
         sym.play(0);

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 250, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 7000, function(sym, e) {
         sym.play(0);

      });
      //Edge binding end

   })("head1");
   //Edge symbol end:'head1'

   //=========================================================
   
   //Edge symbol: 'avatar2'
   (function(symbolName) {   
   
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 250, function(sym, e) {
         sym.play(0);

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 3000, function(sym, e) {
         sym.play(0);

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 6500, function(sym, e) {
         sym.play(0);

      });
      //Edge binding end

   })("avatar2");
   //Edge symbol end:'avatar2'

   //=========================================================
   
   //Edge symbol: 'papel'
   (function(symbolName) {   
   
   })("papel");
   //Edge symbol end:'papel'

   //=========================================================
   
   //Edge symbol: 'btn_launch'
   (function(symbolName) {   
   
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1500, function(sym, e) {
         sym.play(0);

      });
      //Edge binding end

   })("btn_launch");
   //Edge symbol end:'btn_launch'

})(window.jQuery || AdobeEdge.$, AdobeEdge, "EDGE-361118490");